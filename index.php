<?php 

require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");

echo $sheep->get_name(); // "shaun"
echo "<br>";
echo $sheep->get_legs(); // 2
echo "<br>";
echo $sheep->get_cold_blooded(); // false
echo "<br>";


$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "<br>";
echo $kodok->get_legs();


?>